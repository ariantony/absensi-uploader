<?php

namespace App\Http\Controllers;

use App\Models\Absensi;
use App\Models\TabelLog;
use App\Models\Universitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class PostDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Http::post((env('URL_LOGIN')), [
            'email' => env('USER_API'),
            'password' => env('USER_PASS'),
        ])->json();

        $token = $response['token'];
        $data = DB::table('tbl_log')
                    ->where('datetime','like',DB::RAW("CONCAT(DATE_FORMAT(now(),'%Y-%m-%d'),'%')"))
                    ->get()->toArray();
        $url = env('URL_UPLOAD');

        //$postdata = Http::dd()->withToken($token)->post($url, $data);
        $postdata = Http::withToken($token)->post($url, $data);
        return $postdata;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
