<?php

namespace App\Console\Commands;

use App\Models\Universitas;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class AbsensiSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sinkronisasi Data Absensi';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::post('http://travel-haji.test/api/login', [
            'email' => 'tony@gmail.com',
            'password' => 'asdf0987',
        ])->json();

        $token = $response['token'];
        $data = Universitas::all('name','description')->toArray();

        $url = 'http://travel-haji.test/api/absensi';

        Http::withToken($token)->post($url, $data);
    }
}
