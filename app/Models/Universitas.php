<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Universitas extends Model
{
    use HasFactory;

    public $table = 'universities';
    public $timestamps = false;

    protected $fillable = [

    ];

    protected $guarded = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'datetime' => 'datetime:Y-m-d H:i:s'
    ];
}
