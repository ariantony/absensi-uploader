<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TabelLog extends Model
{
    use HasFactory;
    // protected $connection = 'mysql_api';

    public $table = 'tbl_log';
    public $timestamps = false;

    protected $fillable = [

    ];

    protected $guarded = [

    ];

    protected $hidden = [

    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'datetime' => 'datetime:Y-m-d H:i:s'
    ];
}
